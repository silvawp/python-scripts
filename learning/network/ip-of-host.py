#!/usr/bin/python

import sys
import socket

if len(sys.argv) != 2:
	print "Usage: ", sys.argv[0], "host-name"
	sys.exit(1);

host = sys.argv[1]

print socket.gethostbyname(host)

